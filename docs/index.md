![](assets/images/docs.png)

# Premiers pas

# FAQ

# Guides

# Tutos

* [Import et synchro GitHub - GitLab](https://forge.apps.education.fr/laurent.abbal/import-et-synchro-github-gitlab)
* [Utiliser la Forge avec VSCodium ou Visual Studio Code](https://forge.apps.education.fr/laurent.abbal/utiliser-la-forge-avec-vscodium-ou-visual-studio-code)

# Modèles

* [Diaporama en Markdown](https://forge.aeif.fr/modeles-projets/diaporama-en-markdown)
* [Modèle Jekyll](https://forge.aeif.fr/modeles-projets/modele-jekyll)
* [Modèle Pandoc](https://forge.aeif.fr/modeles-projets/modele-pandoc)
* [Site web de cours à usage général](https://forge.aeif.fr/modeles-projets/mkdocs-simple-review)
* [Site web de cours avec exercices Python dans le navigateur](https://forge.aeif.fr/modeles-projets/mkdocs-pyodide-review)
